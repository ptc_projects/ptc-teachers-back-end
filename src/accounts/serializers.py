import functools

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from accounts import models


def hash_password_decorator(method):
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        updated_user = method(self, *args, **kwargs)
        validated_data = kwargs.get('validated_data', args[-1])
        if 'password' in validated_data:
            updated_user.set_password(validated_data['password'])
            updated_user.save()
        return updated_user
    return wrapper


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BasicUser
        fields = ('id', 'email', 'username', 'password', 'gitlab_token')
        extra_kwargs = {'password': {'write_only': True}, 'gitlab_token': {'write_only': True}}

    @hash_password_decorator
    def create(self, validated_data):
        return super().create(validated_data)

    @hash_password_decorator
    def update(self, instance, validated_data):
        return super().update(instance, validated_data)

    @hash_password_decorator
    def partial_update(self, instance, validated_data):
        return super().partial_update(instance, validated_data)


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        return token
