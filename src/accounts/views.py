from django.shortcuts import render
from rest_framework_simplejwt.views import TokenObtainPairView
from accounts.serializers import CustomTokenObtainPairSerializer
from rest_framework import viewsets, permissions
from accounts import models, serializers, permissions as custom_permissions
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status


class UserViewSet(viewsets.ModelViewSet):
    queryset = models.BasicUser.objects.all()
    serializer_class = serializers.UserSerializer

    def get_permissions(self):
        if self.action in ['retrieve', 'update', 'partial_update']:
            permission_classes = (custom_permissions.IsAdminOrIsSelf,)
        else:
            permission_classes = (permissions.IsAdminUser,)

        return [permission() for permission in permission_classes]


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer
