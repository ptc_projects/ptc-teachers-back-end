from django.urls import path, include
from rest_framework_simplejwt.views import TokenRefreshView
from accounts import views
from rest_framework import routers


router = routers.DefaultRouter()
router.register('users', views.UserViewSet)


urlpatterns = [
    path(r'token/', views.CustomTokenObtainPairView.as_view()),
    path(r'token/refresh/', TokenRefreshView.as_view()),
    path(r'', include('rest_framework.urls')),
]
