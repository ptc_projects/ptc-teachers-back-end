from django.contrib.auth.models import AbstractUser
from django.db import models


class BasicUser(AbstractUser):
    gitlab_token = models.CharField(max_length=100)
