from django.apps import AppConfig


class GitlabLogicConfig(AppConfig):
    name = 'gitlab_logic'
