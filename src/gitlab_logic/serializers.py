from gitlab_logic import models
from gitlab_logic.helpers.helper_functions import check_gitlab_group_owner_access
from rest_framework import serializers


class GitLabGroupSerializer(serializers.ModelSerializer):
    owner = serializers.HiddenField(write_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = models.GitlabGroup
        fields = (
            'id', 'name', 'link', 'owner', 'docker_image_link', 'docker_image_name', 'docker_image_branch',
            'docker_image_path', 'docker_image_return_code', 'student_repo_template_link', 'student_repo_branch',
            'student_repo_path', 'ansible_inventory_file', 'ansible_private_ssh_file', 'ansible_return_code')
        extra_kwargs = {
            'ansible_private_ssh_file': {'write_only': True},
            'name': {'read_only': True},
            'docker_image_return_code': {'read_only': True},
            'ansible_return_code': {'read_only': True}
        }

    def validate(self, data):
        if not check_gitlab_group_owner_access(self.context['request'].user.gitlab_token, data.get('link')):
            raise serializers.ValidationError("Don't have required access rights to specified group")
        if data.get('ansible_private_ssh_file') and not data.get('ansible_inventory_file'):
            raise serializers.ValidationError("fill ansible_inventory_file if you fill ansible_private_ssh_file")
        if data.get('docker_image_name') and data.get('docker_image_link'):
            raise serializers.ValidationError("you can't fill name and path simultaneously")
        if data.get('docker_image_name'):
            if data.get('docker_image_branch') or data.get('docker_image_path'):
                raise serializers.ValidationError("can't fill branch or path if you fill name")
        return super().validate(data)


class GitlabGroupEmptySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GitlabGroup
        fields = ()


class GitlabGroupCheckLinkSerializer(serializers.ModelSerializer):
    link = serializers.CharField(max_length=256, required=True, allow_blank=False, allow_null=False)

    class Meta:
        model = models.GitlabGroup
        fields = ('link',)


class GitLabLogsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GitlabGroup
        fields = ('docker_image_logs', 'docker_image_return_code', 'ansible_logs', 'ansible_return_code')
        extra_kwargs = {
            'docker_image_logs': {'read_only': True},
            'docker_image_return_code': {'read_only': True},
            'ansible_logs': {'read_only': True},
            'ansible_return_code': {'read_only': True}
        }
