from gitlab_logic.helpers.commands_executors.config_runner_executor import ConfigRunnerExecutor
from gitlab_logic.helpers.commands_executors.docker_build_executor import DockerBuildExecutor
from gitlab_logic.helpers.commands_executors.student_repo_create_executor import StudentRepoCreateExecutor
from gitlab_logic.helpers.commands_executors.tests_repo_create_executor import TestsRepoCreateExecutor
from gitlab_logic.helpers.commands_executors.variables_fill_executor import VariablesFillExecutor
from ptc_teachers_django.celery import app


@app.task
def build_docker_image(gitlab_group_id: int) -> None:
    DockerBuildExecutor(gitlab_group_id).execute_commands()


@app.task
def create_student_repo(gitlab_group_id: int) -> None:
    StudentRepoCreateExecutor(gitlab_group_id).execute_commands()


@app.task
def create_tests_repo(gitlab_group_id: int) -> None:
    TestsRepoCreateExecutor(gitlab_group_id).execute_commands()


@app.task
def fill_variables(gitlab_group_id: int) -> None:
    VariablesFillExecutor(gitlab_group_id).execute_commands()


@app.task
def config_custom_runner(gitlab_group_id: int) -> None:
    ConfigRunnerExecutor(gitlab_group_id).execute_commands()
