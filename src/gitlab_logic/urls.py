from gitlab_logic import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('groups', views.GitLabGroupViewSet, basename='groups')
