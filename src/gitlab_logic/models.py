from accounts.models import BasicUser
from django.db import models


class GitlabGroup(models.Model):
    name = models.CharField(max_length=256, default='')
    link = models.CharField(max_length=256)
    owner = models.ForeignKey(BasicUser, on_delete=models.CASCADE)

    docker_image_link = models.CharField(max_length=256, blank=True, null=True)
    docker_image_name = models.CharField(max_length=256, blank=True, null=True)
    docker_image_branch = models.CharField(max_length=28, blank=True, null=True)
    docker_image_path = models.CharField(max_length=256, blank=True, null=True)
    docker_image_logs = models.TextField(default="")
    docker_image_return_code = models.IntegerField(blank=True, null=True)

    student_repo_template_link = models.CharField(max_length=256)
    student_repo_branch = models.CharField(max_length=28, blank=True, null=True)
    student_repo_path = models.CharField(max_length=256, blank=True, null=True)

    ansible_inventory_file = models.FileField(upload_to='ansible_inventory_files', blank=True, null=True)
    ansible_private_ssh_file = models.FileField(upload_to='ansible_private_ssh_files', blank=True, null=True)
    ansible_logs = models.TextField(default="")
    ansible_return_code = models.IntegerField(blank=True, null=True)

    @property
    def name(self):
        return self.link.split('/')[-2]
