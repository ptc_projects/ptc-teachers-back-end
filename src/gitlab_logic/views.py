from gitlab_logic import models, serializers
from gitlab_logic.helpers.helper_functions import check_gitlab_group_owner_access
from gitlab_logic.serializers import GitLabLogsSerializer
from gitlab_logic.serializers import GitlabGroupEmptySerializer, GitlabGroupCheckLinkSerializer
from gitlab_logic.tasks import build_docker_image, create_student_repo, create_tests_repo, fill_variables, \
    config_custom_runner
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response


class GitLabGroupViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.GitLabGroupSerializer

    def get_queryset(self):
        if not self.request.user.is_anonymous:
            return models.GitlabGroup.objects.filter(owner=self.request.user)
        return models.GitlabGroup.objects.none()

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        build_docker_image.delay(response.data.get('id'))
        create_tests_repo.delay(response.data.get('id'))
        fill_variables.delay(response.data.get('id'))
        config_custom_runner.delay(response.data.get('id'))
        return response

    def partial_update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if request.data.get('docker_image_name') or request.data.get('docker_image_link'):
            build_docker_image.delay(response.data.get('id'))
        fill_variables.delay(response.data.get('id'))
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        build_docker_image.delay(response.data.get('id'))
        fill_variables.delay(response.data.get('id'))
        return response

    @action(methods=['GET'], detail=True, serializer_class=GitlabGroupEmptySerializer)
    def process_access_requests(self, request, pk=None):
        create_student_repo.delay(pk)
        return Response()

    @action(methods=['POST'], detail=False, serializer_class=GitlabGroupCheckLinkSerializer)
    def check_gitlab_group_access(self, request):
        result = check_gitlab_group_owner_access(gitlab_token=request.user.gitlab_token, link=request.data.get('link'))
        return Response({'status': str(result)})

    @action(methods=['GET'], detail=True, serializer_class=GitlabGroupEmptySerializer)
    def get_logs(self, request, pk=None):
        return Response(data=GitLabLogsSerializer(self.get_object()).data)
