# Generated by Django 3.1.7 on 2021-05-12 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gitlab_logic', '0003_auto_20210502_2315'),
    ]

    operations = [
        migrations.AddField(
            model_name='gitlabgroup',
            name='ansible_return_code',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='gitlabgroup',
            name='docker_image_return_code',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
