import logging

import gitlab
from gitlab_logic.helpers.commands_executors.abstract_cmd_executor import AbstractCmdExecutor
from gitlab_logic.helpers.helper_functions import create_variable
from gitlab_logic.models import GitlabGroup


class VariablesFillExecutor(AbstractCmdExecutor):
    def __init__(self, gitlab_group_id: int):
        super().__init__()

        self.gitlab_group = GitlabGroup.objects.get(pk=gitlab_group_id)
        self.model_instance = self.gitlab_group
        self.logs_field_name = ''

    def execute_commands(self):
        logging.info('START FILLING VARIABLES FOR GROUP')

        gitlab_token = self.gitlab_group.owner.gitlab_token

        create_variable(self.gitlab_group, 'ACCESS_TOKEN', gitlab_token)
        create_variable(self.gitlab_group, 'IS_DIAGNOSTIC', 'diagnostic')
        with gitlab.Gitlab(url='https://gitlab.com', private_token=gitlab_token) as gl:
            gl.auth()
            create_variable(self.gitlab_group, 'ACCESS_TOKEN_USERNAME', gl.user.username)

        logging.info('FINISHED FILLING VARIABLES FOR GROUP')
