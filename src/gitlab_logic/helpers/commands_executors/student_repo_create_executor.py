import logging
import os
import re
import tempfile

import gitlab
from gitlab_logic.helpers.commands_executors.abstract_cmd_executor import AbstractCmdExecutor
from gitlab_logic.helpers.commands_executors.command_status_enum import CommandStatus
from gitlab_logic.helpers.helper_functions import get_link_without_end_slash, collect_commit_files
from gitlab_logic.models import GitlabGroup


class StudentRepoCreateExecutor(AbstractCmdExecutor):
    def __init__(self, gitlab_group_id: int):
        super().__init__()

        self.gitlab_group = GitlabGroup.objects.get(pk=gitlab_group_id)
        self.model_instance = self.gitlab_group
        self.logs_field_name = ''

    def _process_access_requests(self, access_requests_list, commit_files_list, group, gl):
        for access_request in access_requests_list:
            user_id = access_request.id
            user_name = access_request.name
            new_project_name = f'{self.gitlab_group.name} - {user_name}'
            projects_names = {project.name for project in group.projects.list()}

            if new_project_name not in projects_names:
                access_request.delete()
                logging.info(f'DELETED ACCESS REQUEST OF USER {user_name}')
                try:
                    new_project = gl.projects.create({
                        'name': new_project_name,
                        'namespace_id': group.id
                    })
                    logging.info(f'CREATED PROJECT {new_project_name}')
                except gitlab.exceptions.GitlabCreateError as e:
                    logging.warning(f'FAILED TO CREATE PROJECT: {e}')
                    continue
                try:
                    new_project.members.create({
                        'user_id': user_id,
                        'access_level': gitlab.DEVELOPER_ACCESS
                    })
                    logging.info(f'ADDED MEMBER TO PROJECT {new_project_name}')
                except gitlab.exceptions.GitlabCreateError as e:
                    logging.warning(f'FAILED TO ADD MEMBER TO PROJECT {new_project_name}, ERROR MSG: {e}')
                    continue
                try:
                    new_project.commits.create(commit_files_list)
                    logging.info(f'PUSHED COMMIT WITH COLLECTED FILES TO PROJECT {new_project_name}')
                except gitlab.exceptions.GitlabCreateError as e:
                    logging.warning(f'FAILED TO COMMIT COLLECTED FILES TO PROJECT {new_project_name}, ERROR MSG: {e}')
                    continue
            else:
                logging.info(f'USER {access_request.name} ALREADY HAS REPOSITORY')

    def execute_commands(self):
        logging.info('START PROCESSING STUDENTS REPO REQUESTS')

        gitlab_token = self.gitlab_group.owner.gitlab_token
        gitlab_group_link = get_link_without_end_slash(self.gitlab_group.link)
        gitlab_group_basename = os.path.basename(gitlab_group_link)

        with gitlab.Gitlab(url='https://gitlab.com', private_token=gitlab_token) as gl:
            group = gl.groups.get(id=gitlab_group_basename)

            access_requests_list = group.accessrequests.list()
            if len(access_requests_list) == 0:
                return

            with tempfile.TemporaryDirectory() as tmpdir:
                repo_for_clone_link = get_link_without_end_slash(self.gitlab_group.student_repo_template_link)

                tmpdir_abs_path = os.path.abspath(tmpdir)
                repo_dir = os.path.basename(repo_for_clone_link)
                dir_with_content = self.gitlab_group.student_repo_path
                repo_abs_path = os.path.join(tmpdir_abs_path, repo_dir)

                re_compile_for_link_clone = re.compile(r'https?://')
                link_for_clone_command = re_compile_for_link_clone.sub(
                    '',
                    string=repo_for_clone_link
                ).strip().strip('/')

                self.command_list = [
                    (CommandStatus.CHANGE_CWD_COMMAND, tmpdir_abs_path),
                    (CommandStatus.EXECUTABLE_COMMAND,
                     f'git clone https://gitlab.com:{gitlab_token}@{link_for_clone_command}'),
                    (CommandStatus.CHANGE_CWD_COMMAND, repo_abs_path),
                    (CommandStatus.EXECUTABLE_COMMAND, f'git checkout {self.gitlab_group.student_repo_branch}')
                ]

                self._execute_command_list()

                commit_files_list = collect_commit_files(os.path.join(repo_abs_path, dir_with_content))
                self._process_access_requests(access_requests_list, commit_files_list, group, gl)
        logging.info('FINISHED PROCESSING ACCESS REQUESTS')
