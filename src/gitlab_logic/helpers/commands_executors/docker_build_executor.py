import logging
import os
import tempfile

import gitlab
from gitlab_logic.helpers.commands_executors.abstract_cmd_executor import AbstractCmdExecutor
from gitlab_logic.helpers.commands_executors.command_status_enum import CommandStatus
from gitlab_logic.helpers.helper_functions import create_variable
from gitlab_logic.models import GitlabGroup


class DockerBuildExecutor(AbstractCmdExecutor):
    def __init__(self, gitlab_group_id: int):
        super().__init__()

        self.gitlab_group = GitlabGroup.objects.get(pk=gitlab_group_id)
        self.model_instance = self.gitlab_group
        self.logs_field_name = 'docker_image_logs'

    def _create_empty_project(self, project_name):
        with gitlab.Gitlab("https://gitlab.com/", private_token=self.gitlab_group.owner.gitlab_token) as gl:
            groups = list(
                filter(
                    lambda x: '-'.join(x.attributes['name'].lower().split()) == self.gitlab_group.name,
                    gl.groups.list()
                )
            )
            if len(groups) == 1:
                try:
                    gl.projects.create({"name": project_name, "namespace_id": groups[0].id})
                except gitlab.exceptions.GitlabCreateError as e:
                    logging.info(e)
            else:
                logging.warning('Can not find group with specified name')
                return False
            return True

    def execute_commands(self):
        logging.info('START PROCESSING DOCKER IMAGE TASK')

        with tempfile.TemporaryDirectory() as tmpdir:
            abs_path = os.path.abspath(tmpdir)
            docker_image_name = self.gitlab_group.docker_image_name

            if self.gitlab_group.docker_image_link:
                project_name = 'test_runner'
                docker_image_name = f"registry.gitlab.com/{self.gitlab_group.name}/{project_name}/{project_name}"
                dockerfile_path = os.path.join(
                    abs_path,
                    'docker_image',
                    self.gitlab_group.docker_image_path if self.gitlab_group.docker_image_path else ''
                )

                empty_project_status = self._create_empty_project(project_name)
                if not empty_project_status:
                    return

                self.command_list = [
                    (CommandStatus.CHANGE_CWD_COMMAND, abs_path),
                    (CommandStatus.EXECUTABLE_COMMAND, f'git clone {self.gitlab_group.docker_image_link} docker_image'),
                    (CommandStatus.CHANGE_CWD_COMMAND, os.path.join(abs_path, 'docker_image')),
                    (CommandStatus.EXECUTABLE_COMMAND,
                     f'git checkout {self.gitlab_group.docker_image_branch if self.gitlab_group.docker_image_branch else "master"}'),
                    (CommandStatus.EXECUTABLE_COMMAND,
                     f'docker login registry.gitlab.com -p {self.gitlab_group.owner.gitlab_token} -u nickname'),
                    (CommandStatus.EXECUTABLE_COMMAND, f'docker build -t {docker_image_name} {dockerfile_path}'),
                    (CommandStatus.EXECUTABLE_COMMAND, f'docker push {docker_image_name}'),
                    (CommandStatus.EXECUTABLE_COMMAND, f'docker rmi {docker_image_name}')
                ]

                self.gitlab_group.docker_image_logs = ''
                self.gitlab_group.docker_image_return_code = None
                self.gitlab_group.save()

                return_code = self._execute_command_list()

                self.gitlab_group.docker_image_name = docker_image_name
                self.gitlab_group.docker_image_return_code = return_code
                self.gitlab_group.save()

        create_variable(self.gitlab_group, 'PROJECT_IMAGE', docker_image_name)
        logging.info('FINISHED PROCESSING DOCKER IMAGE TASK')
