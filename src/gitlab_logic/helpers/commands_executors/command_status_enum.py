import enum


class CommandStatus(enum.Enum):
    EXECUTABLE_COMMAND = 'EXECUTABLE_COMMAND'
    CHANGE_CWD_COMMAND = 'CHANGE_CWD_COMMAND'
