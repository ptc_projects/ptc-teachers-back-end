import abc
import logging
import subprocess
from typing import Optional

from gitlab_logic.helpers.commands_executors.command_status_enum import CommandStatus


class AbstractCmdExecutor(abc.ABC):
    def __init__(self):
        self.command_list = []
        self.model_instance = None
        self.logs_field_name = ''

    @abc.abstractmethod
    def execute_commands(self):
        pass

    def _write_cmd_execution_logs(
            self,
            proc_stdout: Optional[bytes],
            proc_stderr: Optional[bytes],
            proc_return_code: int
    ):
        if len(self.logs_field_name) > 0:
            current_logs_attr_value = getattr(self.model_instance, self.logs_field_name)
            logs_append_value = '\n'.join([
                proc_stdout.decode('utf-8') if proc_stdout else '',
                proc_stderr.decode('utf-8') if proc_stderr else ''
            ])
            setattr(self.model_instance, self.logs_field_name, current_logs_attr_value + logs_append_value)
            self.model_instance.save()

        logging_with_priority = logging.info

        if proc_return_code != 0:
            logging_with_priority = logging.warning

        logging_with_priority(f'RETURN CODE {proc_return_code}')
        logging_with_priority('STDOUT')
        logging_with_priority(proc_stdout.decode('utf-8') if proc_stdout else '')
        logging_with_priority('STDERR')
        logging_with_priority(proc_stderr.decode('utf-8') if proc_stderr else '')

    def _execute_command_list(self):
        cwd = ''
        return_code = 0

        for idx, status_cmd in enumerate(self.command_list):
            current_cmd_status = status_cmd[0]
            current_cmd_str = status_cmd[1]

            if current_cmd_status is CommandStatus.CHANGE_CWD_COMMAND:
                logging.info(f'CHANGING CWD TO: {current_cmd_str}')
                cwd = current_cmd_str
            else:
                logging.info(f'EXECUTING: {current_cmd_str}')
                process = subprocess.run(
                    current_cmd_str,
                    cwd=cwd,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    shell=True
                )
                self._write_cmd_execution_logs(process.stdout, process.stderr, process.returncode)
                if process.returncode != 0:
                    return_code = process.returncode
                    break

        return return_code
