# PTC Custom GitLab Runner Ansible Configuration

## Short description

This repository contains Ansible configuration files for custom GitLab Runner for PTC project.
This runner is used to move tasks checking logic to server side instead of using `.gitlab-ci.yaml` file in students' repositories.

## Constraints

The current version of configuration works only for Ubuntu distributions. It's tested only for Ubuntu 20.04 version.

## Installation guide

_Pre-caution: you need to run all commands below locally. You don't need to log-in to target host to install and configure this runner._

1. Install Ansible: [official installation guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
1. Clone this repository
   ```
   git clone git@gitlab.com:python_tasks_checker_demo/ptc-custom-runner-ansible-config.git
   cd ptc-custom-runner-ansible-config
   ```
1. Configure `hosts.yaml` file to define IP-address and credentials of target host where custom runner will be installed and configured. You can copy `hosts.yaml.EXAMPLE` file and redefine some options.  
   To test connection configuration use `ansible main -m ping`. The response should be green and return "pong". 
1. Copy `group_vars/all/vars.yaml.EXAMPLE` file to `group_vars/all/vars.yaml`.  
   Replace `<< PUT GITLAB GROUP TOKEN HERE >>` text to group token from `GitLab group -> Settings -> CI/CD -> Runners` in this file.
1. Install dependencies: `ansible-galaxy install -r requirements.yml`
1. Configure target host: `ansible-playbook server_init.yml`
1. 
    - If you want your students to use only this runner, put [this code](gitlab_ci_files/.gitlab-ci.yml.ONLY_CUSTOM) to the `.gitlab-ci.yml` file in students' repositories.  
    - If you want to have the ability to switch to shared GitLab runners if this runner accidentally goes down, put [this code](gitlab_ci_files/.gitlab-ci.yml.SHARED_SUPPORT) to the `.gitlab-ci.yml` file in students' repositories.
1. Disable shared runners in `GitLab group -> Settings -> CI/CD -> Runners`. If you choose second option in the previous step and your server goes down, enable shared runners in the same section.

## Rewriting and updating logic

Server-side logic after triggering CI/CD job is located in `roles/tasks-checker-runner/files/scripts/run.sh` file in `build_script` section.  
Current implementation checks folder and branch name of submission, clones repository with predefined tests and runs docker container with testing logic.  

If you want to rewrite logic of running tests, update and build new docker image ([repository link](https://gitlab.com/python_tasks_checker_demo/python_tasks_checker)).  

The logic before and after running docker image can be changed [here](roles/tasks-checker-runner/files/scripts/run.sh) (and [here](gitlab_ci_files/.gitlab-ci.yml.SHARED_SUPPORT) if you support shared runners).  
Then run command `ansible-playbook server_init.yml -t scripts` to update it in your server.
