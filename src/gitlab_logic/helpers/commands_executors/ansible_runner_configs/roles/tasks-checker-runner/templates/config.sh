#!/usr/bin/env bash

cat << EOS
{
  "builds_dir": "/home/gitlab-runner/builds/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "cache_dir": "/home/gitlab-runner/cache/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "builds_dir_is_shared": true,
  "hostname": "{{ inventory_hostname }}",
  "driver": {
    "name": "unienv task checker",
    "version": "v1.0.0"
  }
}
EOS