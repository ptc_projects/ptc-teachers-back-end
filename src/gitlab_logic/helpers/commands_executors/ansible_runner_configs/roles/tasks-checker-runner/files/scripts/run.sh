#!/bin/bash 

SCRIPT_PATH=$1
SCRIPT_TYPE=$2

source $(dirname "$0")/base.sh

function control_c {
    echo -en "\n## Caught SIGINT; Clean up and Exit \n"
    source $(dirname "$0")/cleanup.sh
    exit $?
}
trap control_c SIGKILL
trap control_c SIGTERM

# https://docs.gitlab.com/runner/executors/custom.html#run
case $SCRIPT_TYPE in
    "prepare_script" )
        # Simple debug info on which machine the Job is running on.
        /bin/bash $1
    ;;
    "get_sources" )
        # Prepares the Git config, and clone/fetch the repository. 
        # We suggest you keep this as is since you get all of the benefits of Git strategies that GitLab provides.
        /bin/bash $1
    ;;
    "restore_cache" )
        # Extract the cache if any are defined. This expects the gitlab-runner binary is available in $PATH.
        /bin/bash $1
    ;;
    "download_artifacts" )
        # Download artifacts, if any are defined. This expects gitlab-runner binary is available in $PATH.
        /bin/bash $1
    ;;
    "build_script" )
        # This is a combination of before_script and script.
        cd $CUSTOM_ENV_CI_PROJECT_DIR
        export MR_BRANCH=$CUSTOM_ENV_CI_BUILD_REF_NAME
        if [ ! -d "${MR_BRANCH}" ]; then
            echo "There is no folder for submission ${MR_BRANCH}" && exit 1;
        fi
        git clone https://${CUSTOM_ENV_ACCESS_TOKEN_USERNAME}:${CUSTOM_ENV_ACCESS_TOKEN}@gitlab.com/${CUSTOM_ENV_TEST_DATA_REPOSITORY} test_data
        
        if [ ! -d "test_data/${MR_BRANCH}" ]; then
            echo "There are no tests for submission ${MR_BRANCH}" && exit 1;
        fi
        docker pull ${CUSTOM_ENV_PROJECT_IMAGE}
        docker run --rm -v $(pwd):/src --env IS_DIAGNOSTIC=${CUSTOM_ENV_IS_DIAGNOSTIC} --env SUBMISSION_NAME=${MR_BRANCH} --env GITLAB_ACCESS_TOKEN=${CUSTOM_ENV_ACCESS_TOKEN} --env PROJECT_ID=${CUSTOM_ENV_CI_MERGE_REQUEST_PROJECT_ID} --env MERGE_REQUEST_IID=${CUSTOM_ENV_CI_MERGE_REQUEST_IID} --env GITLAB_USER_NAME="${CUSTOM_ENV_GITLAB_USER_NAME}" --env PIPELINE_CONFIG_FILENAME=${CUSTOM_ENV_PIPELINE_CONFIG_FILENAME} ${CUSTOM_ENV_PROJECT_IMAGE}
    ;;
    "after_script" )
        # This is the after_script defined from the job. This is always called even if any of the previous steps failed.
        rm -rf
    ;;
    "archive_cache" )
        # Will create an archive of all the cache, if any are defined.
        # /bin/bash $1
    ;;
    "upload_artifacts_on_success" )
        # Upload any artifacts that are defined. Only executed when build_script was successful.
        # /bin/bash $1
    ;;
    "upload_artifacts_on_failure" )
        # Upload any artifacts that are defined. Only exected when build_script fails.
        # /bin/bash $1
    ;;
esac
