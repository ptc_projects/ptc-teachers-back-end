import logging
import os
from urllib.parse import urlparse

import gitlab
from gitlab_logic.helpers.commands_executors.abstract_cmd_executor import AbstractCmdExecutor
from gitlab_logic.helpers.helper_functions import get_link_without_end_slash, collect_commit_files, create_variable
from gitlab_logic.models import GitlabGroup


TESTS_REPO_NAME = 'tests_repository'


class TestsRepoCreateExecutor(AbstractCmdExecutor):
    def __init__(self, gitlab_group_id: int):
        super().__init__()

        self.gitlab_group = GitlabGroup.objects.get(pk=gitlab_group_id)
        self.model_instance = self.gitlab_group
        self.logs_field_name = ''

    def execute_commands(self):
        logging.info('START PROCESSING CREATING REPO WITH TESTS')

        gitlab_token = self.gitlab_group.owner.gitlab_token
        gitlab_group_link = get_link_without_end_slash(self.gitlab_group.link)
        gitlab_group_basename = os.path.basename(gitlab_group_link)

        with gitlab.Gitlab(url='https://gitlab.com', private_token=gitlab_token) as gl:
            group = gl.groups.get(id=gitlab_group_basename)

            if TESTS_REPO_NAME in {project.name for project in group.projects.list()}:
                logging.info('REPO WITH TESTS ALREADY EXISTS')
                return

            commit_files_list = collect_commit_files(
                os.path.join(
                    os.path.dirname(os.path.abspath(__file__)),
                    'tests_repo_init_content'
                )
            )
            new_project = gl.projects.create({
                'name': TESTS_REPO_NAME,
                'namespace_id': group.id
            })
            new_project.commits.create(commit_files_list)
            create_variable(self.gitlab_group, 'TEST_DATA_REPOSITORY', urlparse(new_project.web_url).path[1:])
            logging.info('FINISHED CREATING REPO WITH TESTS')
