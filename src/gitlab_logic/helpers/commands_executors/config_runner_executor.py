import logging
import os
import shutil
import tempfile

import gitlab
from django.template.loader import render_to_string
from gitlab_logic.helpers.commands_executors.abstract_cmd_executor import AbstractCmdExecutor
from gitlab_logic.helpers.commands_executors.command_status_enum import CommandStatus
from gitlab_logic.helpers.helper_functions import get_link_without_end_slash
from gitlab_logic.models import GitlabGroup


class ConfigRunnerExecutor(AbstractCmdExecutor):
    def __init__(self, gitlab_group_id: int):
        super().__init__()

        self.gitlab_group = GitlabGroup.objects.get(pk=gitlab_group_id)
        self.model_instance = self.gitlab_group
        self.logs_field_name = 'ansible_logs'

    def execute_commands(self):
        logging.info('START CONFIGURING CUSTOM RUNNER')

        gitlab_token = self.gitlab_group.owner.gitlab_token
        gitlab_group_link = get_link_without_end_slash(self.gitlab_group.link)
        gitlab_group_basename = os.path.basename(gitlab_group_link)

        with gitlab.Gitlab(url='https://gitlab.com', private_token=gitlab_token) as gl:
            group = gl.groups.get(id=gitlab_group_basename)

            with tempfile.TemporaryDirectory() as tmpdir:
                tmpdir_abs_path = os.path.abspath(tmpdir)

                shutil.copytree(
                    os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ansible_runner_configs'),
                    tmpdir_abs_path,
                    dirs_exist_ok=True
                )
                with open(
                        os.path.join(tmpdir_abs_path, 'group_vars', 'all', 'vars.yaml'),
                        mode='w', encoding='utf-8'
                ) as vars_file:
                    vars_file.write(str(render_to_string(
                        os.path.join('gitlab_logic', 'vars.yaml'),
                        {'runner_token': group.runners_token})
                    ))
                shutil.copyfile(
                    self.gitlab_group.ansible_inventory_file.path,
                    os.path.join(tmpdir_abs_path, 'hosts.yaml')
                )
                if self.gitlab_group.ansible_private_ssh_file:
                    shutil.copyfile(
                        self.gitlab_group.ansible_private_ssh_file.path,
                        os.path.join(tmpdir_abs_path, 'test_ptc')
                    )

                self.command_list = [
                    (CommandStatus.CHANGE_CWD_COMMAND, tmpdir_abs_path),
                    (CommandStatus.EXECUTABLE_COMMAND, 'chmod 700 test_ptc'),
                    (CommandStatus.EXECUTABLE_COMMAND, 'ansible-galaxy install -r requirements.yml'),
                    (CommandStatus.EXECUTABLE_COMMAND, 'ansible-playbook server_init.yml')
                ]
                return_code = self._execute_command_list()
                self.gitlab_group.ansible_return_code = return_code
                self.gitlab_group.save()

        logging.info('FINISHED CONFIGURING CUSTOM RUNNER')
