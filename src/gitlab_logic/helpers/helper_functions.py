import os
import re

import gitlab
from gitlab_logic.models import GitlabGroup


def get_link_without_end_slash(link):
    slash_pattern = r"\/$"
    slash_pattern_compile = re.compile(slash_pattern)
    link = slash_pattern_compile.sub('', link)

    return link


def collect_commit_files(path: str) -> dict:
    commit_actions_dict = {
        "branch": "master",
        "commit_message": "Init commit",
        "actions": [
        ]
    }

    for root, dirs, files in os.walk(path):
        for file in files:
            directory = root.replace(path, '')
            result = {
                "action": "create",
                "file_path": os.path.join(directory, file),
                "content": open(os.path.join(root, file)).read(),
            }

            commit_actions_dict['actions'].append(result)

    return commit_actions_dict


def create_variable(gitlab_group_model_instance: GitlabGroup, variable_key, variable_value):
    with gitlab.Gitlab("https://gitlab.com/", private_token=gitlab_group_model_instance.owner.gitlab_token) as gl:
        groups = list(
            filter(
                lambda x: '-'.join(x.attributes['name'].lower().split()) == gitlab_group_model_instance.name,
                gl.groups.list()
            )
        )
        if len(groups) == 1:
            remote_gitlab_group = groups[0]
            try:
                gitlab_variable = remote_gitlab_group.variables.get(variable_key)
                gitlab_variable.value = variable_value
            except gitlab.exceptions.GitlabGetError:
                gitlab_variable = remote_gitlab_group.variables.create({
                    'key': variable_key,
                    'value': variable_value,
                    'protected': False,
                    'masked': True
                })
            gitlab_variable.save()


def check_gitlab_group_owner_access(gitlab_token: str, link: str):
    link = get_link_without_end_slash(link)
    gitlab_group_basename = os.path.basename(link)

    with gitlab.Gitlab(private_token=gitlab_token, url='https://gitlab.com') as gl:
        try:
            group = gl.groups.get(id=gitlab_group_basename)
        except gitlab.GitlabGetError:
            return False

        group_users = group.members.list()

        gl.auth()

        for user in group_users:
            if user.id == gl.user.id and user.access_level == 50:
                return True

        return False
