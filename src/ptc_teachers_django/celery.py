import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ptc_teachers_django.settings')

app = Celery('ptc_teachers_django')
app.config_from_object('django.conf:settings')

app.autodiscover_tasks()
