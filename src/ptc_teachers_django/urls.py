"""ptc_teachers_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import gitlab_logic
from django.contrib import admin
from django.urls import path
from django.urls import path, include
from accounts import views
from accounts.urls import router as auth_router
from rest_framework import routers
from gitlab_logic.urls import router as gitlab_logic_router


router = routers.DefaultRouter()
router.registry.extend(auth_router.registry)
router.registry.extend(gitlab_logic_router.registry)


urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'api/auth/', include('accounts.urls')),
    path(r'api/', include(router.urls))
]
